declare global {
    interface Window {
        tinyMCE: any;
    }
}

export default class ConformisAdmin {
    public static CONFORMIS_BUTTON_TEXT: string = "conformis_button-text";
    public static CONFORMIS_LANGUAGES: string = "conformis_languages";
    public static CONFORMIS_MESSAGE_ID: string = "conformis_message";

    private readonly dropdown: HTMLSelectElement;
    private readonly gdprButtonText: HTMLInputElement;
    private gdprMessage: any;

    constructor() {
        this.dropdown = document.querySelector(`#${ConformisAdmin.CONFORMIS_LANGUAGES}__dropdown`) as HTMLSelectElement;
        this.gdprButtonText = document.querySelector(`#${ConformisAdmin.CONFORMIS_BUTTON_TEXT}`) as HTMLInputElement;
        this.gdprButtonText.addEventListener("change", (e: Event) =>
            this.setSettingFor(ConformisAdmin.CONFORMIS_BUTTON_TEXT, (e.target as HTMLInputElement)!.value)
        );

        const data = this.getSettings();
        this.gdprMessage?.setContent(data.message);
        this.gdprButtonText.value = data.buttonText;

        // Fires when an editor is added to the EditorManager collection.
        window!.tinyMCE.on("addeditor", (e: any) => {
            const editor = e.editor;
            if (editor.id === ConformisAdmin.CONFORMIS_MESSAGE_ID) {
                this.gdprMessage = editor;
                editor.targetElm.removeAttribute("name");
                editor.on("loadContent", () => editor.setContent(this.getSettings().message));
                editor.on("change", (ev: any) =>
                    this.setSettingFor(ConformisAdmin.CONFORMIS_MESSAGE_ID, ev.level.content)
                );
            }
        });
    }

    protected getSettings(): { buttonText: string; message: string } {
        return {
            buttonText: (
                (document.querySelector(
                    `[name="conformis_options[${ConformisAdmin.CONFORMIS_BUTTON_TEXT}]"]`
                ) as HTMLInputElement) || { value: "" }
            ).value,
            message: ConformisAdmin.unescapeHtml(
                (
                    (document.querySelector(
                        `[name="conformis_options[${ConformisAdmin.CONFORMIS_MESSAGE_ID}]"]`
                    ) as HTMLInputElement) || { value: "" }
                ).value
            ),
        };
    }

    protected setSettingFor(setting: string, value: string): this {
        const languageSetting = document.querySelector(`[name="conformis_options[${setting}]"]`) as HTMLInputElement;
        if (languageSetting) {
            languageSetting.value = ConformisAdmin.escapeHtml(value);
        }
        return this;
    }

    public static escapeHtml(html: string): string {
        const map: { [key: string]: string } = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#039;",
        };

        return html.replace(/[&<>"']/g, (m: string) => {
            return map[m];
        });
    }

    public static unescapeHtml(html: string): string {
        const map: { [key: string]: string } = {
            "&amp;": "&",
            "&lt;": "<",
            "&gt;": ">",
            "&quot;": '"',
            "&#039;": "'",
        };

        return html.replace(/&[^;]+;/g, (m: string) => {
            return map[m];
        });
    }
}
