import Conformis from "./conformis";

import "../css/styles.scss";

document.addEventListener("DOMContentLoaded", () => {
    const conformis = new Conformis();
    window.setTimeout(() => conformis.showBanner(), 500);
});
