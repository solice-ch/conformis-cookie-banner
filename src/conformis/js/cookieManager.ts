export default class CookieManager {
    public static CONFORMIS_CONSENT_COOKIE: string = "conformis_cookie_consent";
    public static CONFORMIS_COOKIE_DURATION: number = 365;

    protected constructor() {}

    public static allowCookies(value: string): void {
        CookieManager.set(CookieManager.CONFORMIS_CONSENT_COOKIE, value, CookieManager.CONFORMIS_COOKIE_DURATION);
    }

    public static hasConsent(): boolean {
        return !!CookieManager.get(CookieManager.CONFORMIS_CONSENT_COOKIE);
    }

    public static set(name: string, value: string, days: number): void {
        let expires = "";
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = `${name}=${value}${expires}; path=/`;
    }

    public static get(name: string): string | undefined {
        const cookie = CookieManager.getAll().filter(c => c.key === name);
        return cookie.length > 0 ? cookie[0].value : undefined;
    }

    public static getAll(): { key: string; value: string }[] {
        return document.cookie
            .split(";")
            .map(cookie => cookie.trim())
            .map(cookie => cookie.split("="))
            .map(cookiePair => ({ key: cookiePair[0], value: cookiePair[1] }));
    }

    public static remove(name: string): void {
        CookieManager.set(name, "", -1);
    }
}
