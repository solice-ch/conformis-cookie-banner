import CookieManager from "./cookieManager";

export default class Conformis {
    public static CONFORMIS_PLUGIN_REF: string = ".conformis-plugin";

    private readonly banner?: HTMLDivElement;
    private readonly confirmButton?: HTMLButtonElement;

    public constructor() {
        this.banner = document.querySelector(`${Conformis.CONFORMIS_PLUGIN_REF}__wrapper`) as HTMLDivElement;
        this.confirmButton = this.banner.querySelector(
            `${Conformis.CONFORMIS_PLUGIN_REF}__btn--confirm`
        ) as HTMLButtonElement;
        this.confirmButton.addEventListener("click", () => this.confirmBanner());
    }

    public confirmBanner(): this {
        CookieManager.allowCookies("yes");
        this.closeBanner();
        return this;
    }

    public closeBanner(): this {
        this.banner?.classList.add("closed");
        return this;
    }

    public showBanner(): this {
        if (!CookieManager.hasConsent()) {
            this.banner?.classList.remove("closed");
        }
        return this;
    }
}
