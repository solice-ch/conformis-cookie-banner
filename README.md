# Conformis Cookie Banner

## Project setup
```
yarn install
```

### Compiles with source-map for development
```
yarn build --mode=development
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
