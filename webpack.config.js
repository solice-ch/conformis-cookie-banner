const path = require("path");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const ZipPlugin = require("zip-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");
const postcssImport = require("postcss-import");

module.exports = (env, argv) => {
    const mode = argv.mode || "production";
    const pluginName = "conformis";

    return {
        mode,
        entry: {
            main: `./src/${pluginName}/js/main.ts`,
            admin: `./src/${pluginName}/js/admin.ts`,
        },
        resolve: {
            modules: [path.resolve(__dirname, "./node_modules")],
            extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
        },
        output: {
            filename: `js/[name].js`,
            path: path.resolve(__dirname, `dist/${pluginName}`),
        },
        devtool: mode === "production" ? false : "source-map",
        plugins: [
            new MiniCssExtractPlugin({
                filename: "css/[name].css",
                chunkFilename: "css/[name].css",
            }),
            new ForkTsCheckerWebpackPlugin({
                async: false,
                checkSyntacticErrors: true,
                tsconfig: path.join(__dirname, "tsconfig.json"),
                tslint: path.join(__dirname, "tslint.json"),
            }),
            new CopyPlugin([
                {
                    from: path.resolve(__dirname, `src/${pluginName}`),
                    to: path.resolve(__dirname, `dist/${pluginName}`),
                    ignore: ["*.ts", "*.tsx", "*.js", "*.jsx", "*.css", "*.scss", "*.sass"],
                },
            ]),
            new ZipPlugin({
                filename: pluginName,
                path: path.resolve(__dirname, `dist`),
            }),
        ],
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "ts-loader",
                        options: {
                            transpileOnly: true,
                        }
                    },
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                        },
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: mode !== "production",
                            },
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                plugins: [postcssImport(), autoprefixer()],
                            },
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: mode !== "production",
                            }
                        },
                    ],
                },
            ],
        },
        optimization: {
            minimize: mode === "production",
        },
    }
};